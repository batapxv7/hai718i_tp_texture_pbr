# Install script for directory: /run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "libassimp5.2.5" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libassimpd.so.5.2.5"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libassimpd.so.5"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/cmake-build-debug/thirdparties/assimp/bin/libassimpd.so.5.2.5"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/cmake-build-debug/thirdparties/assimp/bin/libassimpd.so.5"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libassimpd.so.5.2.5"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libassimpd.so.5"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "libassimp5.2.5" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libassimpd.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libassimpd.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libassimpd.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/cmake-build-debug/thirdparties/assimp/bin/libassimpd.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libassimpd.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libassimpd.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libassimpd.so")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "assimp-dev" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp" TYPE FILE FILES
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/anim.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/aabb.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/ai_assert.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/camera.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/color4.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/color4.inl"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/cmake-build-debug/thirdparties/assimp/code/../include/assimp/config.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/ColladaMetaData.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/commonMetaData.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/defs.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/cfileio.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/light.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/material.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/material.inl"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/matrix3x3.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/matrix3x3.inl"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/matrix4x4.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/matrix4x4.inl"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/mesh.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/ObjMaterial.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/pbrmaterial.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/GltfMaterial.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/postprocess.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/quaternion.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/quaternion.inl"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/scene.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/metadata.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/texture.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/types.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/vector2.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/vector2.inl"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/vector3.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/vector3.inl"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/version.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/cimport.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/importerdesc.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Importer.hpp"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/DefaultLogger.hpp"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/ProgressHandler.hpp"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/IOStream.hpp"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/IOSystem.hpp"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Logger.hpp"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/LogStream.hpp"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/NullLogger.hpp"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/cexport.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Exporter.hpp"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/DefaultIOStream.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/DefaultIOSystem.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/ZipArchiveIOSystem.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/SceneCombiner.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/fast_atof.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/qnan.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/BaseImporter.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Hash.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/MemoryIOWrapper.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/ParsingUtils.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/StreamReader.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/StreamWriter.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/StringComparison.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/StringUtils.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/SGSpatialSort.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/GenericProperty.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/SpatialSort.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/SkeletonMeshBuilder.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/SmallVector.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/SmoothingGroups.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/SmoothingGroups.inl"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/StandardShapes.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/RemoveComments.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Subdivision.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Vertex.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/LineSplitter.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/TinyFormatter.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Profiler.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/LogAux.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Bitmap.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/XMLTools.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/IOStreamBuffer.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/CreateAnimMesh.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/XmlParser.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/BlobIOSystem.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/MathFunctions.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Exceptional.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/ByteSwapper.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Base64.hpp"
    )
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "assimp-dev" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp/Compiler" TYPE FILE FILES
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Compiler/pushpack1.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Compiler/poppack1.h"
    "/run/media/batap/HDD_2To/Dossier_Bat/Fac_CV/fac/m1/Prog3D/TP/prog-3D-2022/template/thirdparties/assimp/code/../include/assimp/Compiler/pstdint.h"
    )
endif()

